# PROJECT NAME - ex1 
  
  
## Repository Name - ex1

### Assignment 1

## Description
 
Install apache web server on Ubuntu and configure it to host two take fake website on two ports.
<http://localhost:8081> <http://localhost:8082>
 
## Process

Step-1 : Install apache2 using the following command **sudo apt-get install apache2**.
 
Step-2 : Goes into **/var/www** directory.
   
Step-3 : Create a new directory(The name of my newly created directory is **meen**).
 
Step-4 : Go to newly created directory using the command of **cd meen** then i have created two separate directory **abc** and 
       **def**.created html file(**index.html**)on both directory **abc and def** separately.
      
Step-5 :Next go to **/etc/apache2/sites-available** and create a two configuration files(**file1.conf and file2.conf**) and 
       setting  **000.default.conf file1.conf** for port 8081 and **000-default.conf file2.conf** for port 8082.
       
Step-6 : Edit the ports.conf file to listen the ports 8081 and 8082.
  
Step-7 : Open the firefox and type **localhost:8081** and **localhost:8082** and see what are the contents given in that 
      html files.
      

 
 
### Assignment_2
    
## Description
   
Write a bash shell script to backup any specified directory on a daily,weekly,monthly basis to remote server.</p>

## Process
  
Step-1 :I have Created a script **back.sh**.
   
Step-2 : Write a code inside the script for backup the files from specified directory on a daily,weekly,monthly basis.
The **back.sh** script automatically backup the files.
       
Step-3 : Run the **back.sh** file and see the result.
  
  
    
  
### Assignement 3
    
## Description
   
Write a Make file to build a sample project with five C source files.Demonstrate building the project using GCC and LLVM compilers  
    
## Process

    
Step-1 :I have Created five c files and one header file(**add.c,hello.c,sub.c,multiply.c,main.c,header.h**).
    
Step-2 : Create Makefile using the command **gedit Makefile**.
    
Step-3 : **final** and **output** is a target names,the first target is compiled in **gcc** compiler and anthor target is 
compiled in **clang** compiler.
     
Step-4 :Run the Makefile using **make** command.
     
Finally see the output using **./final** and **./output** commands.
     
     

### Assignment 4

## Description
   
Design a GUI form for student registration using any technology like Qt,Java,Python.
    
## Process

I have designed a Registration form using the technology Qt creater.
   
In my Registration form i have added so many fields like label,textedit,line edit,scroll bar,radio button,checkbox,push button,
group box and etc.
   
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
