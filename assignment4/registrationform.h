#ifndef REGISTRATIONFORM_H
#define REGISTRATIONFORM_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class RegistrationForm; }
QT_END_NAMESPACE

class RegistrationForm : public QMainWindow
{
    Q_OBJECT

public:
    RegistrationForm(QWidget *parent = nullptr);
    ~RegistrationForm();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::RegistrationForm *ui;
};
#endif // REGISTRATIONFORM_H
